<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 */

namespace Mayhem\Failure;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

/**
 * Class RedirectFailure
 * @package Mayhem\Failure
 *
 * Failure-Actionclass redirecting your user to where he TRULY wants to be
 */
class RedirectFailure implements FailureInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @type string
     */
    const PARAM_REDIRECTTO = "redirectTo";

    /**
     * RedirectFailure constructor.
     */
    public function __construct()
    {
        $this->logger = new NullLogger();
    }

    /**
     * Executes the failure-action: redirects the request
     *
     * @param array $parameters
     */
    public function __invoke(array $parameters = [])
    {
        $redirectTo = 'http://www.theuselessweb.com/';
        if (isset($parameters[self::PARAM_REDIRECTTO])) {
            $redirectTo = $parameters[self::PARAM_REDIRECTTO];
        }
        $this->logger->debug("Redirecting the user far away from here...");
        header(sprintf('Location: %s', $redirectTo));
    }
}
