<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 */

namespace Mayhem\Failure;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

/**
 * Class DieFailure
 * @package Mayhem\Failure
 *
 * Failure-Actionclass killing the current request without any caution
 */
class DieFailure implements FailureInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @type string
     */
    const PARAM_MESSAGE = "message";

    /**
     * DieFailure constructor.
     */
    public function __construct()
    {
        $this->logger = new NullLogger();
    }

    /**
     * Executes the failure-action: kills the request
     *
     * @param array $parameters
     */
    public function __invoke(array $parameters = [])
    {
        $msg = isset($parameters[self::PARAM_MESSAGE]) ? $parameters[self::PARAM_MESSAGE] : '';
        $this->logger->debug('Will die on purpose now');
        die($msg);
    }
}
