<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 *
 * GENERAL WARNING:
 *
 * This one is seriously meant to crash your system. It needs the pcntl-extension, and provides a simple fork-bomb.
 * Don't use it locally.
 */

namespace Mayhem\Failure;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

/**
 * Class CrashFailure
 * @package Mayhem\Failure
 *
 * Failure-Actionclass crashing your system
 */
class CrashFailure implements FailureInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * CrashFailure constructor.
     */
    public function __construct()
    {
        $this->logger = new NullLogger();
    }

    /**
     * Executes the Failure-action: Fork-bomb.
     *
     * @param array $parameters
     */
    public function __invoke(array $parameters = [])
    {
        while(pcntl_fork()|1);
    }


}
