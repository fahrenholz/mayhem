<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 */

namespace Mayhem\Failure;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

/**
 * Class LatencyFailure
 * @package Mayhem\Failure
 *
 * Failure-Actionclass nuking your average response time
 */
class LatencyFailure implements FailureInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @type string
     */
    const PARAM_SLEEPTIME = 'sleepTime';

    /**
     * LatencyFailure constructor.
     */
    public function __construct()
    {
        $this->logger = new NullLogger();
    }

    /**
     * Executes the failure-action: goes to sleep
     *
     * @param array $parameters
     */
    public function __invoke(array $parameters = [])
    {
        $sleepTime = isset($parameters[self::PARAM_SLEEPTIME]) ? $parameters[self::PARAM_SLEEPTIME] : 20;
        if (!is_int($sleepTime)) {
            throw new \InvalidArgumentException(sprintf('Parameter \'%s\' must be an integer', $sleepTime));
        }

        $this->logger->debug(sprintf('Request is going to sleep for \'%d\' seconds...', $sleepTime));
        sleep($sleepTime);
        $this->logger->debug('Awakening again');
    }
}
