<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 */

namespace Mayhem\Failure;

/**
 * Interface FailureInterface
 * @package Mayhem\Failure
 *
 * Common interface for all Failure-Actionclasses
 */
interface FailureInterface
{
    /**
     * Executes the failure action
     *
     * @param array $parameters
     *
     * @return mixed
     */
    public function __invoke(array $parameters = []);
}
