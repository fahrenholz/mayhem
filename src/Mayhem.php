<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 */

namespace Mayhem;

use Mayhem\Entity\Event;
use Mayhem\Resolver\ProbabilityResolver;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

/**
 * Class Mayhem
 * @package Mayhem
 *
 * Service class enabling you to instantiate a bunch of probabilities, roll a dice and get back an event
 */
class Mayhem implements LoggerAwareInterface
{
    use LoggerAwareTrait;
    /**
     * @var ProbabilityResolver
     */
    private $resolver;

    /**
     * Mayhem constructor.
     * @param array $probabilities
     */
    public function __construct(array $probabilities = [])
    {
        $this->resolver = new ProbabilityResolver($probabilities);
        $this->logger = new NullLogger();
    }

    /**
     * Initiates a dice roll to pick a probability of something to fail. Returns the probability-event. Returns null
     * if nothing happens
     *
     * @return null|Event
     */
    public function rollADice(): ?object
    {
        $probability = $this->resolver->pickOne();
        if ($probability === null) {
            return null;
        }

        $this->logger->info(
            sprintf(
                'Lightning has struck: mayhem will occur and take the shape of \'%s\'',
                $probability->getName()
            ),
            [
                "probability" => $probability
            ]
        );

        return new Event($probability->getFailureClass(), $probability->getParameters());
    }
}
