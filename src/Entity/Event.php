<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 */

namespace Mayhem\Entity;

use Mayhem\Failure\FailureInterface;

/**
 * Class Event
 * @package Mayhem\Entity
 *
 * Event-entity
 */
class Event
{
    /**
     * @var string
     */
    private $failureAction;

    /**
     * @var array
     */
    private $parameters;

    /**
     * Event constructor.
     * @param string $failureAction
     * @param array $parameters
     */
    public function __construct(string $failureAction, array $parameters = [])
    {
        $this->failureAction = $failureAction;
        $this->parameters = $parameters;
    }

    /**
     * Getter for the failureAction-Class name
     *
     * @return string
     */
    public function getFailureActionClassName(): string
    {
        return $this->failureAction;
    }

    /**
     * Builds the FailureAction and returns it
     *
     * @return FailureInterface
     */
    public function getFailureAction(): FailureInterface
    {
        return new $this->failureAction();
    }

    /**
     * Getter for the parameters
     *
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * Builds the failureAction and invokes it, returning its eventual result
     *
     * @return mixed
     */
    public function __invoke()
    {
        $action = $this->getFailureAction();
        return $action($this->getParameters());
    }
}