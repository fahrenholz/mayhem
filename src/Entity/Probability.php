<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 */

namespace Mayhem\Entity;

/**
 * Class Probability
 * @package Mayhem\Entity
 *
 * Entity describing and Event and it's probability
 */
class Probability
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $probability;

    /**
     * @var string
     */
    private $failureClass;

    /**
     * @var array
     */
    private $parameters;

    /**
     * Probability constructor
     *
     * @param string $name
     * @param float $probability
     * @param string $failureClass
     * @param array $parameters
     */
    public function __construct(string $name, float $probability, string $failureClass, array $parameters = [])
    {
        $this->name = $name;
        $this->setProbability($probability);
        $this->failureClass = $failureClass;
        $this->parameters = $parameters;
    }

    /**
     * Getter for name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Setter for name
     *
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Getter for probability
     *
     * @return float
     */
    public function getProbability(): float
    {
        return $this->probability;
    }

    /**
     * Setter for probability
     *
     * @param float $probability
     */
    public function setProbability(float $probability): void
    {
        if ($probability < 0.0 || $probability > 1.0) {
            throw new \InvalidArgumentException('A probability must be contained between 0.0 and 1.0');
        }

        $this->probability = $probability;
    }

    /**
     * Getter for failure-class-name
     *
     * @return string
     */
    public function getFailureClass(): string
    {
        return $this->failureClass;
    }

    /**
     * Setter for failure-class-name
     *
     * @param string $failureClass
     */
    public function setFailureClass(string $failureClass): void
    {
        $this->failureClass = $failureClass;
    }

    /**
     * Getter for event parameters
     *
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * Setter for event parameters
     *
     * @param array $parameters
     */
    public function setParameters(array $parameters): void
    {
        $this->parameters = $parameters;
    }

    /**
     * Formats as string
     *
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            'Name: %s, Probability: %f, Action Class: %s',
            $this->name,
            $this->probability,
            $this->failureClass
        );
    }
}
