<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 */

namespace Mayhem\Resolver;

use Codeception\Util\Debug;
use Mayhem\Entity\Probability;

/**
 * Class ProbabilityResolver
 * @package Mayhem\Resolver
 *
 * Class holding and validating the probabilities in total and picking one randomly
 */
class ProbabilityResolver
{
    /**
     * @var Probability[]
     */
    private $probabilities;

    /**
     * @var Probability[]
     */
    private $topBoundaries = [];

    /**
     * ProbabilityResolver constructor.
     * @param Probability[] $probabilities
     */
    public function __construct(array $probabilities = [])
    {
        $total = 0.0;
        foreach ($probabilities as $probability) {
            if (! $probability instanceof Probability) {
                throw new \InvalidArgumentException(
                    sprintf(
                        'Probabilities must be of class \'%s\'',
                        Probability::class
                    )
                );
            }

            $total = $total + $probability->getProbability();
        }

        if ($total > 1.0) {
            throw new \InvalidArgumentException(sprintf(
                'Total probabilities must be lesser or equal than 1.0, %f given',
                $total
            ));
        }

        $this->probabilities = $probabilities;
        $this->calculateTopBoundaries();
    }

    /**
     * Randomly picks a number and then picks a probability according to that number. Returns null if nothing happens
     *
     * @return Probability|null
     */
    public function pickOne(): ?Probability
    {
        $rand = rand(0, 100);
        $result = null;
        $boundaryKeys = array_keys($this->topBoundaries);

        $previousKey = -1;
        for ($i = 0; $i < count($this->topBoundaries); $i++) {
            $currentKey=$boundaryKeys[$i];
            if ($rand > $previousKey && $rand <= $currentKey) {
                $result = $this->topBoundaries[$currentKey];
            }
            $previousKey = $currentKey;
        }

        return $result;
    }

    /**
     * Rearranges the probabilities in an indexed array based on their chance of happening
     */
    private function calculateTopBoundaries(): void
    {
        $currentBoundary = 0.0;
        foreach ($this->probabilities as $probability) {
            $currentBoundary = $currentBoundary + $probability->getProbability();
            $index = (int) ($currentBoundary * 100);
            if (!isset($this->topBoundaries[$index])) {
                $this->topBoundaries[$index] = $probability;
            }
        }
    }
}
