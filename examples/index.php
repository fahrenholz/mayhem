<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 */

require __DIR__.'/../vendor/autoload.php';

$logger = new Monolog\Logger("log");

$probabilities = [
    new \Mayhem\Entity\Probability(
        'dieProb',
        0.2,
        \Mayhem\Failure\DieFailure::class,
        [
            \Mayhem\Failure\DieFailure::PARAM_MESSAGE => "<p>I hear you talk girl<br/>".
                "Now your conscience is clear</p>" . PHP_EOL,
        ]
    ),
    new \Mayhem\Entity\Probability(
        'sleepProb',
        0.2,
        \Mayhem\Failure\LatencyFailure::class
    ),
    new \Mayhem\Entity\Probability(
        'redirectProb',
        0.2,
        \Mayhem\Failure\RedirectFailure::class
    )
];

$lib = new \Mayhem\Mayhem($probabilities);
$lib->setLogger($logger);

$event = $lib->rollADice();
if ($event !== null) {
    $failure = $event->getFailureAction();

    if ($failure instanceof \Psr\Log\LoggerAwareInterface) {
        $failure->setLogger($logger);
    }

    $failure($event->getParameters());
}

$lines = require('./lines.php');

?>

<html>
    <head>
        <title>I will survive</title>
    </head>
    <style>
        body {
            background-color: #77b8ff;
        }
        div {
            margin: 10% auto;
            width: 50%;
            border: 1px solid #ccc;
            text-align:center;
            background-color: #fff;
        }
        p {
            padding: 30px;
            font-size: 2em;
            font-weight:bold;
        }
    </style>
    <body>
        <div>
            <p>
                ...<br/>
    <?php
    foreach ($lines as $line) {
        echo $line.'<br/>';
    }
    ?>
                ...
            </p>
        </div>
    </body>
</html>
