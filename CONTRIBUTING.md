# Styleguide

Symfony2 Standard applies

# Phpdoc-blocs

## File Template

```php
/**
* This file is part of fahrenholz/mayhem
* (c) Vincent Fahrenholz 2018
* Licence: M.I.T
* Author: Firstname Lastname <email@example.com>
*/
```

## Class, interface and trait Template
```php
/**
 * Class ${NAME}
 * @package namespace
 *
 * Description
 */
```

of course, change class accordingly

## Field template 
```php
/**
 * @var Type
 */
```

## Function template
```php
/**
 * Description
 *
 * @param type $name
 *
 * @return type 
 * @throws type
 */
```