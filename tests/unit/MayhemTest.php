<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 */

namespace Mayhem;

use Mayhem\Entity\Probability;
use Psr\Log\NullLogger;

/**
 * Class MayhemTest
 * @package Mayhem
 *
 * Test for the main service
 */
class MayhemTest extends \Codeception\Test\Unit
{
    /**
     * @var \Mayhem\UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

// tests
    public function testRollADiceOnEmptyServiceShouldReturnNull()
    {
        $service = new Mayhem();
        $this->assertEquals(null, $service->rollADice());
    }

    public function testRollADiceOnFilledProbabilityTotalShouldGiveBackAccurateActionAndParameters()
    {
        $service = new Mayhem([ new Probability('test', 1.0, 'test', [
            'myParam' => 'lala',
        ])]);
        $this->assertEquals('test', $service->rollADice()->getFailureActionClassName());
        $this->assertEquals(['myParam' => 'lala'], $service->rollADice()->getParameters());
    }

    public function testServiceCreationWithTotalProbabilityOverOneShouldFail()
    {
        $this->expectException(\InvalidArgumentException::class);
        $probs = [
            new Probability('test', 0.6, 'testClass'),
            new Probability('test', 0.6, 'testClass'),
        ];

        new Mayhem($probs);
    }

    public function testSetLoggerShouldNotBreakAnything()
    {
        $logger = new NullLogger();
        $service = new Mayhem();
        $service->setLogger($logger);

        $this->assertTrue(true);
    }
}