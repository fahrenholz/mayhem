<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 */

namespace Mayhem\Resolver;

use Codeception\Util\Debug;
use Mayhem\Entity\Probability;

/**
 * Class ProbabilityResolverTest
 * @package Mayhem\Resolver
 *
 * Test for the ProbabilityResolver
 */
class ProbabilityResolverTest extends \Codeception\Test\Unit
{
    /**
     * @var \Mayhem\UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testPickOneOnEmptyResolverShouldReturnNull()
    {
        $resolver = new ProbabilityResolver();
        $this->assertEquals(null, $resolver->pickOne());
    }

    public function testPickOneOnResolverWithFullProbabilitiesEqualToOneShouldReturnProbability()
    {
        $probs = [
            new Probability('test', 0.0, 'testClass'),
            new Probability('test', 1.0, 'testClass'),
        ];
        $resolver = new ProbabilityResolver($probs);
        $this->assertInstanceOf(Probability::class, $resolver->pickOne());
    }

    public function testResolverWithTotalProbabilityOverOneShouldFail()
    {
        $this->expectException(\InvalidArgumentException::class);
        $probs = [
            new Probability('test', 0.6, 'testClass'),
            new Probability('test', 0.6, 'testClass'),
        ];

        new ProbabilityResolver($probs);
    }

    public function testResolverWithArrayOfOtherThingsThanProbabilitiesShouldFail()
    {
        $this->expectException(\InvalidArgumentException::class);
        $probs = [
            new Probability('test', 0.6, 'testClass'),
            "I'm nothing good",
        ];

        new ProbabilityResolver($probs);
    }

    public function testResolverWithTweakedArraysShouldReturnAccurateProbability()
    {
        $probs = [
            new Probability('test1', 1.0, 'testClass'),
            new Probability('test2', 0.0, 'testClass'),
        ];
        $resolver = new ProbabilityResolver($probs);
        $probability = $resolver->pickOne();
        $this->assertEquals('test1', $probability->getName());
    }

    public function testProbabilitySpreadsEqually()
    {
        $probs = [
            new Probability('test1', 0.2, 'testClass'),
            new Probability('test2', 0.2, 'testClass'),
            new Probability('test3', 0.2, 'testClass'),
            new Probability('test4', 0.2, 'testClass'),
            new Probability('test5', 0.2, 'testClass'),
        ];
        $resolver = new ProbabilityResolver($probs);
        $probCount = [
            'test1' => 0,
            'test2' => 0,
            'test3' => 0,
            'test4' => 0,
            'test5' => 0
        ];

        for ($i = 0; $i < 10000; $i++) {
            $probability = $resolver->pickOne()->getName();
            $probCount[$probability] = $probCount[$probability] + 1;
        }

        $total = 0;
        foreach ($probCount as $index => $count) {
            //thresholds: should be between 500 and 4500
            $this->assertTrue($count < 4500 && $count > 500, "$index is not in the range 500 <> 4500");
            $total += $count;
        }
        $this->assertEquals(10000, $total);
    }
}