<?php
/**
 * This file is part of fahrenholz/mayhem
 * (c) Vincent Fahrenholz 2018
 * Licence: M.I.T
 * Author: Vincent Fahrenholz <vincent@fahrenholz.eu>
 */

namespace Mayhem\Entity;

/**
 * Class ProbabilityTest
 * @package Mayhem\Entity
 *
 * Test for the Probability-Entity
 */
class ProbabilityTest extends \Codeception\Test\Unit
{
    /**
     * @var \Mayhem\UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testCreationShouldAcceptValidSetOfParameters()
    {
        $probability = new Probability(
            'test',
            0.1,
            'testClass',
            [
                "test" => "test"
            ]
        );

        $this->assertEquals('test', $probability->getName());
        $this->assertEquals(0.1, $probability->getProbability());
        $this->assertEquals('testClass', $probability->getFailureClass());
        $this->assertEquals(['test' => 'test'], $probability->getParameters());
    }

    public function testProbabilityGreaterThanOneShouldNotBeAccepted()
    {
        $this->expectException(\InvalidArgumentException::class);
        new Probability(
            'test',
            1.1,
            'testClass',
            []
        );
    }

    public function testProbabilityLowerThanZeroShouldNotBeAccepted()
    {
        $this->expectException(\InvalidArgumentException::class);
        new Probability(
            'test',
            -0.34,
            'testClass',
            []
        );
    }

    public function testSetterShouldOverrideValues()
    {
        $probability = new Probability(
            'test',
            0.1,
            'testClass',
            [
                'test' => 'test',
            ]
        );

        $probability->setProbability(0.2);
        $this->assertEquals(0.2, $probability->getProbability());
        $probability->setName('testtest');
        $this->assertEquals('testtest', $probability->getName());
        $probability->setFailureClass('another');
        $this->assertEquals('another', $probability->getFailureClass());
        $probability->setParameters([]);
        $this->assertEquals([], $probability->getParameters());
    }

    public function testToStringShouldReturnString()
    {
        $probability = new Probability('test', 0.1, 'class');
        $this->assertInternalType('string', (string) $probability);
    }
}