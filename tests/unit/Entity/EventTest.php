<?php
namespace Mayhem\Entity;

use Mayhem\Failure\FailureInterface;

class EventTest extends \Codeception\Test\Unit
{
    /**
     * @var \Mayhem\UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testEventShouldReturnTheRightFailureAction()
    {
        $event = new Event(TestFailure::class, []);
        $this->assertInstanceOf(TestFailure::class, $event->getFailureAction());
        $this->assertEquals(TestFailure::class, $event->getFailureActionClassName());
    }

    /**
     * @dataProvider dataProvider
     */
    public function testEventInvocationShouldReturnWhatsExpected($parameters)
    {
        $event = new Event(TestFailure::class, $parameters);
        $this->assertEquals(count($parameters), $event());
    }

    public function dataProvider()
    {
        return [
            [ [] ],
            [[0]],
            [[0,1]],
        ];
    }
}

class TestFailure implements FailureInterface
{
    public function __invoke(array $parameters = [])
    {
        return count($parameters);
    }
}