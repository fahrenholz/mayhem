# PHP Mayhem Library
[![pipeline status](https://gitlab.com/fahrenholz/mayhem/badges/master/pipeline.svg)](https://gitlab.com/fahrenholz/mayhem/commits/master)
[![coverage report](https://gitlab.com/fahrenholz/mayhem/badges/master/coverage.svg)](https://gitlab.com/fahrenholz/mayhem/commits/master)
[![License](https://poser.pugx.org/fahrenholz/mayhem/license)](https://gitlab.com/fahrenholz/mayhem/blob/master/LICENSE)
[![PHPStan](https://img.shields.io/badge/PHPStan-enabled-brightgreen.svg?style=flat)](https://gitlab.com/fahrenholz/mayhem)

## What problem is solved

The purpose of this library is to allow you, in a framework-agnostic way, to introduce some random failure in your
PHP-Application. Whether it would be to have some fun or to enforce the resiliency of your system, this library allows
you to roll a dice, randomly selecting between a standard set of actions or to create your own actions in order to 
confuse your client as you prefer.

## Getting started

### Installation via composer

    $ composer require fahrenholz/mayhem:^1.0
    
### Example test

    $ cd examples && docker-compose up -d
    
This will launch a docker container with the official php+apache image and set up a small test website
connect to it browsing to [http://localhost:8080](http://localhost:8080)

## Usage

### Out of the box

#### Instantiate
```php
use Mayhem\Entity\Probability;
use Mayhem\Mayhem;
use Mayhem\Failure\LatencyFailure;
use Mayhem\Failure\RedirectFailure;

$probabilities = [
    new Probability('prob1', 0.03, LatencyFailure::class),
    new Probability('prob2', 0.01, RedirectFailure::class),
];

$service = new Mayhem($probabilities);

```

#### Use

```php
$event = $service->rollADice(); //randomly picks an event or nothing
if ($event !== null) {
    //something will happen
    $failure = $event->getFailureAction(); //instantiate failure
    $failure($event->getParameters()); //invoke failure
}
```

##### A word of caution

The CrashFailure shipped with the Library sets loose a forkbomb. Use it with caution in order
not to fully crash your development environment.

### Extending

There is only a limited set of failures shipped with the library, and they are all untested
for a reason: their purpose is to give you a blueprint for implementing your own failure 
scenarios.

There's only one rule to do that: Every failure scenario must implement FailureInterface. 

## Contribute

Feel free to contribute and to get in touch with me.
